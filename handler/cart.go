package handler

import (
	"context"
	"gitee.com/includestdio/cart/domain/model"
	"gitee.com/includestdio/cart/domain/service"
	gocategoryservicecart "gitee.com/includestdio/cart/proto"
	"gitee.com/includestdio/common"
)

type Cart struct {
	CartDataService service.ICartDataService
}

func (c *Cart) AddCart(ctx context.Context, in *gocategoryservicecart.CartInfo, out *gocategoryservicecart.ResponseAdd) error {
	cart := &model.Cart{}
	cart.SizeId = in.SizeId
	cart.ProductID = in.ProductId
	cart.UserId = in.UserId
	cartId, err := c.CartDataService.AddCart(cart)
	if err != nil {
		out.CartId = 0
		out.Msg = "添加失败"
		return err
	}
	out.CartId = cartId
	out.Msg = "添加成功"
	return nil
}

func (c *Cart) CleanCart(ctx context.Context, in *gocategoryservicecart.Clean, out *gocategoryservicecart.Response) error {
	err := c.CartDataService.CleanCart(in.UserId)
	if err != nil {
		out.Msg = "清除失败"
		return err
	}
	out.Msg = "清除成功"
	return nil
}
func (c *Cart) Incr(ctx context.Context, in *gocategoryservicecart.Item, out *gocategoryservicecart.Response) error {
	err := c.CartDataService.IncrNum(in.Id, in.ChangeNum)
	if err != nil {
		out.Msg = "添加失败"
		return err
	}
	out.Msg = "添加成功"
	return nil
}
func (c *Cart) Decr(ctx context.Context, in *gocategoryservicecart.Item, out *gocategoryservicecart.Response) error {
	err := c.CartDataService.DecrNum(in.Id, in.ChangeNum)
	if err != nil {
		out.Msg = "减少失败"
		return err
	}
	out.Msg = "添加失败"
	return nil
}
func (c *Cart) DeleteItemById(ctx context.Context, in *gocategoryservicecart.CartId, out *gocategoryservicecart.Response) error {
	err := c.CartDataService.DeleteCart(in.Id)
	if err != nil {
		out.Msg = "删除失败"
		return err
	}
	out.Msg = "删除失败"
	return nil
}

func (c *Cart) GetAll(ctx context.Context, in *gocategoryservicecart.AllRequest, out *gocategoryservicecart.CartAll) error {
	carts, err := c.CartDataService.FindAllCart(in.UserId)
	if err != nil {
		out.AllCartInfos = nil
		return err
	}
	for _, cart := range carts {
		ci := &gocategoryservicecart.CartInfo{}
		if err := common.SwapTo(cart, ci); err != nil {
			out.AllCartInfos = nil
			return err
		}
		out.AllCartInfos = append(out.AllCartInfos, ci)
	}
	return nil
}
