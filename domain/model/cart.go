package model

type Cart struct {
	ID        uint64 `gorm:"primary_key; not_null; auto_increment" json:"id"`
	ProductID uint64 `gorm:"not_null" json:"product_id"`
	Num       uint64 `gorm:"not_null" json:"num"`
	SizeId    uint64 `gorm:"not_null" json:"size_id"`
	UserId    uint64 `gorm:"not_null" json:"user_id"`
}
