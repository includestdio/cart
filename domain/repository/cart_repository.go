package repository

import (
	"errors"
	"gitee.com/includestdio/cart/domain/model"
	"gorm.io/gorm"
)

type ICartRepository interface {
	InitTable() error
	FindCartById(uint64) (*model.Cart, error)
	CreateCart(*model.Cart) (uint64, error)
	DeleteCartById(uint64) error
	UpdateCart(*model.Cart) error
	FindAll(uint64) ([]model.Cart, error)
	CleanCart(uint64) error
	IncrNum(uint64, uint64) error
	DecrNum(uint64, uint64) error
}
type CartRepository struct {
	mysqlDb *gorm.DB
}

func (c CartRepository) CleanCart(userId uint64) error {
	return c.mysqlDb.Where("user_id = ?", userId).Delete(&model.Cart{}).Error
}

func (c CartRepository) IncrNum(cartId uint64, num uint64) error {
	cart := &model.Cart{ID: cartId}
	return c.mysqlDb.Model(cart).UpdateColumn("num", gorm.Expr("num + ?", num)).Error
}

func (c CartRepository) DecrNum(cartId uint64, num uint64) error {
	cart := &model.Cart{ID: cartId}
	result := c.mysqlDb.Model(cart).Where("num >= ?", num).UpdateColumn("num", gorm.Expr("num - ?", num))
	if result.Error != nil {
		return result.Error
	}
	if result.RowsAffected == 0 {
		return errors.New("减少失败")
	}
	return nil
}

func (c CartRepository) InitTable() error {
	return c.mysqlDb.AutoMigrate(&model.Cart{})
}

func (c CartRepository) FindCartById(cartId uint64) (cart *model.Cart, err error) {
	return cart, c.mysqlDb.First(&cart, cartId).Error
}

func (c CartRepository) CreateCart(cart *model.Cart) (uint64, error) {
	db := c.mysqlDb.FirstOrCreate(cart, model.Cart{UserId: cart.UserId, ProductID: cart.ProductID, SizeId: cart.SizeId})
	if db.Error != nil {
		return 0, db.Error
	}
	if db.RowsAffected == 0 {
		return 0, errors.New("购物车插入失败")
	}
	return cart.ID, nil
}

func (c CartRepository) DeleteCartById(cartId uint64) error {
	return c.mysqlDb.Where("id = ?", cartId).Delete(&model.Cart{}).Error
}

func (c CartRepository) UpdateCart(cart *model.Cart) error {
	return c.mysqlDb.Model(cart).Updates(cart).Error
}

func (c CartRepository) FindAll(userId uint64) (cartAll []model.Cart, err error) {
	return cartAll, c.mysqlDb.Where("user_id = ?", userId).Find(&cartAll).Error
}

func NewCartRepository(db *gorm.DB) ICartRepository {
	return &CartRepository{mysqlDb: db}
}
