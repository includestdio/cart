package service

import (
	"gitee.com/includestdio/cart/domain/model"
	"gitee.com/includestdio/cart/domain/repository"
)

type ICartDataService interface {
	AddCart(cart *model.Cart) (uint64, error)
	DeleteCart(uint64) error
	UpdateCart(*model.Cart) error
	FindCartById(uint64) (*model.Cart, error)
	FindAllCart(uint64) ([]model.Cart, error)
	CleanCart(uint64) error
	IncrNum(uint64, uint64) error
	DecrNum(uint64, uint64) error
}

type CartDataService struct {
	CartRepository repository.ICartRepository
}

func (c CartDataService) CleanCart(userId uint64) error {
	return c.CartRepository.CleanCart(userId)
}

func (c CartDataService) IncrNum(cartId uint64, num uint64) error {
	return c.CartRepository.IncrNum(cartId, num)
}

func (c CartDataService) DecrNum(cartId uint64, num uint64) error {
	return c.CartRepository.DecrNum(cartId, num)
}

func (c CartDataService) AddCart(cart *model.Cart) (uint64, error) {
	return c.CartRepository.CreateCart(cart)
}

func (c CartDataService) DeleteCart(cartId uint64) error {
	return c.CartRepository.DeleteCartById(cartId)
}

func (c CartDataService) UpdateCart(cart *model.Cart) error {
	return c.CartRepository.UpdateCart(cart)
}

func (c CartDataService) FindCartById(cartId uint64) (*model.Cart, error) {
	return c.CartRepository.FindCartById(cartId)
}

func (c CartDataService) FindAllCart(userId uint64) ([]model.Cart, error) {
	return c.CartRepository.FindAll(userId)
}

func NewCartDataService(cartRepository repository.ICartRepository) ICartDataService {
	return &CartDataService{CartRepository: cartRepository}
}
