
GOPATH:=$(shell go env GOPATH)
.PHONY: init
init:
	go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
	go install micro.dev/v4/cmd/protoc-gen-micro@latest
	go install micro.dev/v4/cmd/protoc-gen-openapi@latest

.PHONY: api
api:
	protoc --openapi_out=. --proto_path=. proto/cart.proto

.PHONY: proto
proto:
	protoc --proto_path=. --micro_out=./proto --go_out=:./proto proto/cart.proto
	
.PHONY: build
build:
	go build -o cart *.go

.PHONY: test
test:
	go test -v ./... -cover

.PHONY: docker
docker:
	docker build . -t cart:latest
