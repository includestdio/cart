package main

import (
	"gitee.com/includestdio/cart/domain/repository"
	service3 "gitee.com/includestdio/cart/domain/service"
	"gitee.com/includestdio/cart/handler"
	service2 "gitee.com/includestdio/cart/proto"
	"gitee.com/includestdio/common"
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2/registry"
	consul2 "github.com/micro/go-plugins/registry/consul/v2"
	"github.com/micro/go-plugins/wrapper/ratelimiter/uber/v2"
	opentracing2 "github.com/micro/go-plugins/wrapper/trace/opentracing/v2"
	"github.com/opentracing/opentracing-go"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"strconv"
)

var QPS = 100

func main() {
	// 配置中心
	consulConfig, err := common.GetConsulConfig("127.0.0.1", 8500, "/micro/config")

	if err != nil {
		logger.Error(err)
	}
	// 注册中心
	consulRegister := consul2.NewRegistry(func(options *registry.Options) {
		options.Addrs = []string{
			"127.0.0.1:8500",
		}
	})

	// 链路追踪
	t, io, err := common.NewTracer("go.micro.service.cart", "localhost:6831")
	if err != nil {
		logger.Error(err)
	}

	defer io.Close()
	opentracing.SetGlobalTracer(t)

	// 数据库连接
	mysqlInfo := common.GetMysqlFromConsul(consulConfig, "mysql")
	dsn := mysqlInfo.User + ":" + mysqlInfo.Pwd + "@tcp(" + mysqlInfo.Host + ":" + strconv.FormatInt(mysqlInfo.Port, 10) + ")/" + mysqlInfo.Database + "?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	// 初始化数据表
	if err := repository.NewCartRepository(db).InitTable(); err != nil {
		logger.Error(err)
	}

	// 微服务地址配置
	service := micro.NewService(
		micro.Name("go.micro.service.cart"),
		micro.Version("latest"),
		// 微服务暴露地址
		micro.Address("0.0.0.0:8087"),
		// 注册中心
		micro.Registry(consulRegister),
		// 链路追踪
		micro.WrapHandler(opentracing2.NewHandlerWrapper(opentracing.GlobalTracer())),
		// 添加限流
		micro.WrapHandler(ratelimit.NewHandlerWrapper(QPS)),
	)

	service.Init()

	cartDataService := service3.NewCartDataService(repository.NewCartRepository(db))

	// Register handler
	if err := service2.RegisterCartHandler(service.Server(), &handler.Cart{CartDataService: cartDataService}); err != nil {
		logger.Fatal(err)
	}

	// Run service
	if err := service.Run(); err != nil {
		logger.Fatal(err)
	}
}
